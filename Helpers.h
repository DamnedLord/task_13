#pragma once

#include <type_traits>

// Вычисление квадрата суммы аргументов
template<typename T>
constexpr auto SquareSum(T Val1, T Val2) // Не такая скучная реализация, как int SquareSum(int, int) ;)
{
	// Проверка переданного тип аргумента на "число"
	static_assert(std::is_arithmetic<T>::value, "Wrong argument type");

	// Сумма и квадрат без создания временной переменной
	Val1 += Val2;
	return Val1 *= Val1;
}