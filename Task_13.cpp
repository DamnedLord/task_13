#include <iostream>

#include "Helpers.h"

int main()
{
    std::cout << SquareSum(12.0, 13.0) << std::endl
        << SquareSum(5, 7) << std::endl;
}